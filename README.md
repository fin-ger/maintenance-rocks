# Maintenance Rocks

This is the source code of the [maintenance.rocks](https://maintenance.rocks/) website.

Visit [fin-ger/offering-maintenance-rocks](https://codeberg.org/fin-ger/offering-maintenance-rocks) for other source code.
